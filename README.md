# Android-sdk

Docker container with an Android build environment. Compatible with
Gitlab/BitBucket CI.

> Note: this container automatically accepts the Android SDK licence(s) on your
> behalf. Please do not use if you are unable to accept the licences.

## Image variants

Docker hub tags are maintained for each major Android release (e.g. Lollipop,
Nougat). New tags are added with API level increases, so `lollipop-22` replaces
`lollipop-21`.

The tags pushed to docker hub can be seen on the
[environments](https://gitlab.com/danhawkes/android-sdk/environments) page.

### `android-sdk/<codename>-<api-level>`

The 'minimal' image. Contains just the platform and build tools.

### `android-sdk/<codename>-<api-level>-google`

As above, but with Google-specific stuff. Includes the `extras`, `google` and
`m2repository` components, with pre-accepted licences.

## Usage

Mount your source code at `/workspace` in the container, and run your `gradlew`
script.

    docker run -v <project>:/workspace danhawkes/android-sdk <command>

    e.g.          /my-project:/workspace                     ./gradlew assemble

## Customisation

Create a custom image with the following docker build args:

* `sdk_url`
* `sdk_components` - Components to install, in the form of a filter string for
  the `android sdk` command.

  A list of available SDK components can be obtained by running this command
  inside the container:

        android list sdk -a -e | grep 'id:' | awk '{print $4}' | sed 's:"::g'

## Release procedure

1.  Update files in master branch (including changes to `update-branches.py`
    script)
2.  Commit changes
3.  Run `./update-branches.py`; this will rebase branches for each docker tag
    off `master`.
4.  Force push modified branches (`git push -f --all`)
