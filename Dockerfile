FROM ubuntu:18.04 as install

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update -q && \
    apt-get install --no-install-recommends -q -y \
    openjdk-8-jdk \
    curl \
    unzip \
    expect \
    ca-certificates

# Download and extract SDK
ARG sdk_url="https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip"
ARG sdk_components="tools platform-tools platforms;android-25"

RUN curl -L -o sdk.zip $sdk_url
RUN unzip -d /sdk sdk.zip

# Accept licences and install components
WORKDIR /sdk/tools/bin
COPY yesffs /
RUN expect /yesffs ./sdkmanager --licenses
RUN expect /yesffs ./sdkmanager $sdk_components

FROM ubuntu:16.04 as run

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update -q && \
    apt-get install --no-install-recommends -q -y \
    openjdk-8-jdk \
    ca-certificates \
    git-core

COPY --from=install /sdk /opt/sdk

ENV ANDROID_SDK=/opt/sdk
ENV ANDROID_HOME=/opt/sdk
ENV PATH=/opt/sdk/tools:/opt/sdk/platform-tools:$PATH

RUN mkdir /workspace
WORKDIR /workspace
