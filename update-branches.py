#!/usr/bin/env python3

import fileinput
import re
from subprocess import call, check_call, check_output, STDOUT

platforms = [{
    'api': 27,
    'codename': 'oreo'
}, {
    'api': 25,
    'codename': 'nougat'
}, {
    'api': 23,
    'codename': 'marshmallow'
}, {
    'api': 22,
    'codename': 'lollipop'
}, {
    'api': 20,
    'codename': 'kitkat'
}]

common_components = ['tools', 'platform-tools']
firebase_version = '10.2.0'
support_version = '25.2.0'
support_v4_lib = 'com.android.support:support-v4:{}'.format(support_version)

variants = [{
    'name': '',
    'sdk_components': common_components,
    'test_libs': [support_v4_lib]
}, {
    'name':
    'google',
    'sdk_components':
    common_components + [
        'extras;google;m2repository',
    ],
    'test_libs': [
        support_v4_lib,
        'com.google.firebase:firebase-core:{}'.format(firebase_version),
        'com.google.firebase:firebase-messaging:{}'.format(firebase_version)
    ]
}]

branches = []
for platform in platforms:
    for variant in variants:
        branch = {}

        # e.g. 23-marshmallow-google
        name = [str(platform['api']), platform['codename']]
        if variant['name']:
            name.append(variant['name'])

        # e.g. android-23
        platform_component = 'android-{}'.format(str(platform['api']))

        sdk_components = list(variant['sdk_components'])
        sdk_components.append('platforms;{}'.format(platform_component))

        branches.append({
            'name': '-'.join(name),
            'sdk_target': platform_component,
            'sdk_components': sdk_components,
            'test_libs': variant['test_libs']
        })


def replace_in_file(path, regex, replacement):
    with fileinput.FileInput(path, inplace=True) as file:
        for line in file:
            print(re.sub(regex, replacement, line), end='')


for branch in branches:

    name = branch['name']
    sdk_components = branch['sdk_components']
    sdk_target = branch['sdk_target']
    test_libs = branch['test_libs']

    # Checkout master, reset any changes to the Dockerfile
    check_call(['git', 'checkout', 'master'])
    check_call(['git', 'checkout', '--', 'Dockerfile'])

    # Delete (if exists) and checkout branch
    call(['git', 'branch', '-D', name])
    check_call(['git', 'checkout', '-b', name])

    # Replace SDK components to the the dockerfile
    replace_in_file('Dockerfile', r'(sdk_components=").*(")',
                    r'\1' + ' '.join(sdk_components) + r'\2')

    # Replace target SDK version in build.gradle
    replace_in_file('test/build.gradle', r'(compileSdkVersion \').*(\')$',
                    r'\1' + sdk_target + r'\2')

    # Add dependencies to test project
    with open('test/build.gradle', 'a') as f:
        f.write('dependencies {\n')
        for test_lib in test_libs:
            f.write('    compile \'' + test_lib + '\'\n')
        f.write('}\n')

    # Commit any changes
    if check_output(['git', 'status', '--porcelain']):
        check_call(['git', 'add', '-A'])
        check_call(['git', 'commit', '-m', 'Update branch'], stderr=STDOUT)
    else:
        # No changes were required
        print('no change')

# Check out master again
check_call(['git', 'checkout', 'master'])
